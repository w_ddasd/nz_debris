package main

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type Config struct {
	FormData FormData `yaml:"formData"`
	Url      string   `yaml:"url"`
	Cookie   string   `yaml:"cookie"`
	Time     int      `yaml:"time"`
}

type FormData struct {
	SServiceType       string `yaml:"sServiceType"`
	IActivityId        string `yaml:"iActivityId"`
	SServiceDepartment string `yaml:"sServiceDepartment"`
	IFlowId            string `yaml:"iFlowId"`
	GTk                string `yaml:"g_tk"`
	SMiloTag           string `yaml:"sMiloTag"`
	ECode              string `yaml:"e_code"`
	GCode              string `yaml:"g_code"`
	EasUrl             string `yaml:"eas_url"`
	EasRefer           string `yaml:"eas_refer"`
	IUin               string `yaml:"iUin"`
	SArea              string `yaml:"sArea"`
	SRole              string `yaml:"sRole"`
	Type               string `yaml:"type"`
}

type responseMsg struct {
	Msg     string      `json:"msg"`
	FlowRet flowRetData `json:"flowRet"`
}

type flowRetData struct {
	IRet string `json:"iRet"`
	SMsg string `json:"sMsg"`
}

var c Config
var logger = logrus.New()

var nzUrl string

var cookie string

func main() {
	formatterLog()
	logger.Info("开始执行程序...")
	// 读取config文件
	initConfig()
	// 配置入参
	postData := configRequest()

	// 创建2秒发送一次
	ticker := time.Tick(time.Duration(c.Time) * time.Millisecond)

	for range ticker {

		// 创建请求
		req, err := http.NewRequest("POST", nzUrl, strings.NewReader(postData.Encode()))
		if err != nil {
			fmt.Println("Error creating request:", err)
			return
		}
		// 设置请求头
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("cookie", cookie)
		req.Header.Set("Host", "comm.ams.game.qq.com")
		req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0")
		logger.Info("开始发送请求...")
		// 发送请求
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			logger.Error("错误的请求发送: ", err)
			return
		}
		defer resp.Body.Close()

		// 读取响应体
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			logger.Error("读取响应体body错误: ", err)
			return
		}
		// 打印响应体
		var respMsg responseMsg
		err = json.Unmarshal([]byte(body), &respMsg)
		if err != nil {
			logrus.Error("body响应体解析错误: ", err)
		}
		logger.Info("请求结果: ", respMsg.Msg)
		logger.Info("请求结果: ", respMsg.FlowRet.SMsg)
	}
}

func configRequest() url.Values {
	nzUrl = c.Url
	cookie = c.Cookie

	postData := url.Values{}
	postData.Add("sServiceType", c.FormData.SServiceType)
	postData.Add("iActivityId", c.FormData.IActivityId)
	postData.Add("sServiceDepartment", c.FormData.SServiceDepartment)
	postData.Add("iFlowId", c.FormData.IFlowId)
	postData.Add("g_tk", c.FormData.GTk)
	postData.Add("sMiloTag", c.FormData.SMiloTag)
	postData.Add("e_code", c.FormData.ECode)
	postData.Add("g_code", c.FormData.GCode)
	postData.Add("eas_url", c.FormData.EasUrl)
	postData.Add("eas_refer", c.FormData.EasRefer)
	postData.Add("iUin", c.FormData.IUin)
	postData.Add("sArea", c.FormData.SArea)
	postData.Add("sRole", c.FormData.SRole)
	postData.Add("type", c.FormData.Type)

	return postData
}

func formatterLog() {
	// 自定义时间格式
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	logger.Formatter = customFormatter
}

func initConfig() {
	logger.Info("开始读取配置文件...")
	data, err := os.ReadFile("config.yml")
	if err != nil {
		logger.Fatal("读取配置文件错误: ", err)
	}
	err = yaml.Unmarshal(data, &c)
	if err != nil {
		logger.Fatal("解析配置文件错误: ", err)
	}
	logger.Info("配置文件读取成功...")
}
