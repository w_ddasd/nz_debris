# 逆战自动小脚本

## 简介

本软件是一个基于Go语言（Golang）开发的自动化脚本，旨在帮助用户在逆战游戏的官方网页中自动使用轮回碎片进行抽奖与轮回之心/轮回碎片兑换功能。

## 运行环境

- **Go语言环境**：确保你的电脑上已安装Go语言环境，版本推荐为最新版本或至少支持当前代码的版本。
- **网络环境**：确保你的设备可以访问逆战游戏的官方网站。

## 安装步骤

**安装Go语言**：访问[Go语言官网](https://golang.org/dl/)下载并安装Go语言。

**克隆项目**：

```go
git clone https://gitee.com/w_ddasd/nz_debris.git  
cd nz_debris
```

## 安装依赖
```go
go mod tidy
```


## 使用说明
1. **登录**：首先，你需要在浏览器中手动登录逆战游戏的官方网站，并保持登录状态。
2. F12获取config.yml所需要的参数
3. 使用教学视频:【逆战自动轮回碎片抽奖,解放双手】https://www.bilibili.com/video/BV1jgtSetEMw?vd_source=9afb9c26c27ec4c3ccbdaa94c74002e0
4. **运行脚本**:
```go
go run main.go


```

## 本次新增

1. 添加使用兑换轮回碎片/轮回之心功能
2. 新增excel文件,内含所有兑换的type编号

## exe文件下载地址

```text
通过百度网盘分享的文件：nz_debris.zip
链接：https://pan.baidu.com/s/1TIlt6znX3sMkypRroSBYmQ?pwd=5ilh 
提取码：5ilh 

```
